package com.project.basic.challenge.service;

import java.util.*;

import com.project.basic.challenge.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.basic.challenge.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User getUser(String str){
        return userRepository.findByLastName(str);
    }

    public void addUser(User user ){
        userRepository.save(user);
    }

    public List<User> getUsers(){
        return userRepository.findAll();
    }

    public void updateUser(long userId,User user){
        User userToUpdate = userRepository.getOne(userId);
        userToUpdate.setId(user.getId());
        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setLastName(user.getLastName());
        userRepository.save(userToUpdate);
    }

    public void deleteUser(long userId){
        userRepository.delete(userId);
    }

    public long countUser(){
        return userRepository.count();
    }

}