package com.project.basic.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTestBasicChallengeApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringTestBasicChallengeApplication.class, args);
    }
}