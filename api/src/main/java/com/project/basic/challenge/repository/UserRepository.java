package com.project.basic.challenge.repository;

import com.project.basic.challenge.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<User,Long> {
    User findByLastName(String lastName);
}